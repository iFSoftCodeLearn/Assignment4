﻿namespace Net.M.A006.Excercise1 {
    internal class Program {
        static void Main(string[] args) {
            int[] arr = { 1, 2, 3, -10, 13, 34, 7 };

            if (!TryGetMinMax(arr, out int max, out int min)) return;
            Console.WriteLine("Maximum is: " + max);
            Console.WriteLine("Minimum is: " + min);
        }

        private static bool TryGetMinMax(int[] arr, out int max, out int min) {
            max = min = 0;
            if (arr == null || arr.Length == 0) return false;
            max = arr.Max(x => x);
            min = arr.Min(x => x);
            return true;    
        }
    }
}