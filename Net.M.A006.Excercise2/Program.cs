﻿namespace Net.M.A006.Excercise2 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(FindGCD(15, 30));
        }

        private static int FindGCD(int n1, int n2) => n2 != 0 ? FindGCD(n2, n1 % n2) : n1;
    }
}