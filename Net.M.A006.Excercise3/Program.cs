﻿namespace Net.M.A006.Excercise3 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(FindGCD(new int[]{ 6,12,24,60}));
        }

        private static int FindGCD(int[] arr) {
            int result = arr[0];
            for (int i = 1; i < arr.Length; i++) {
                result = FindGCD(arr[i], result);

                if (result == 1) return 1;
            }
            return result;
        }

        private static int FindGCD(int n1, int n2) => n2 != 0 ? FindGCD(n2, n1 % n2) : n1;
    }
}